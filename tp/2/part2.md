# [](#tp2Setup-ftp)II. Setup du serveur FTP

## 1. Installation du serveur
### Installation du packet
   

> sudo apt install vsftp

![](img/installftp.png)



## 2. Lancement du service FTP
> sudo systemctl start

![](img/startftp.png)

Vérification du bon lancement du service
> sudo systemctl status

![](img/statusftp.png)

## 3. Etude du service FTP
### **Analyser le service en cours de fonctionnement**
- Afficher le statut du service

![](img/statusftp.png)

- Afficher les processus liés au service

![](img/psftp.png) 

- Afficher les port utilisé par le service

![](img/ssftp.png)

-	Afficher les logs du service

![](img/statusftp.png)

### Se connecter au serveur

![](img/connectionftp.png)

Upload & téléchargement d'un fichier

![](img/ud.png)

Vérifications dans les logs
> sudo cat /var/log/vsftpd.log

![](img/uploadanddownload.png)

## 4. Modification de la configuration du serveur
**Modifier le comportement du service**
Changer le port d'écoute du vstfpd
> sudo nano /etc/vsftpd.conf
> listen_port=42069

![](img/ftpport.png)

Vérifier le port d'écoute

![](img/ssportftp.png)

Se connecter avec un client ftp

![](img/ftpnewport.png)

Upload & download un fichier de nouveaux

![](img/newud.png)
