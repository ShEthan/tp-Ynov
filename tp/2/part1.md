##  II. Setup du serveur SSH
### Installation du serveur

![](img/installssh.png)

### Lancement du service SSH

![](img/startssh.png)

### Etude du service SSH

Afficher le statut du service

![](img/startssh.png)

Afficher les processus liés au service ssh

![](img/psssh.png)

Afficher le port utilisé par le service ssh

![](img/sssh.png)

Afficher les logs du service ssh

![](img/logsshvar.png)

### Se connecter au serveur

![](img/hosttovm.png)

## Modification de la configuration du serveur

### Modifier le comportement du service
![](img/configssh.png)

Verifier le port d'écoute 

![](img/sssh2.png)

**Se connecter à la machine avec le nouveau port**

![](img/connecttovmport.png)
