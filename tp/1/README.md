# [](#tp1-are-you-dead-yet-)TP1 : Are you dead yet ?


## Méthode 1 
**Supprimer tout les fichiers du linux**

```
sudo rm -rf /*
```
## Méthodes 2 
**Empecher d'utiliser les commandes en changeant les permissions.**

```
sudo chmod 000 /*
```

## Méthode 3 
**Faire une boucle de reboot.**

Faire un fichier sh avec à l'intérieur "reboot"

nano reboot.sh
```
reboot
```
faire un fichier das /etc/ appelé "rc.local" 

rc-local permet de lancer des scripts pendant le boot.


sudo nano /etc/rc.local
```
#!/bin/sh
/home/user/reboot.sh
```
Permettre au fichier de s'executer
```
sudo chmod +x /etc/rc.local
```
Lancer le rc.local
```
sydo systemctl start rc-local
```
Le systeme vas reboot en boucle.

## Méthode 4 
**Deplacer le boot**

Deplacer le boot dans un autre ficher afin d'empecher le boot
```
sudo mv /boot/* /
```
