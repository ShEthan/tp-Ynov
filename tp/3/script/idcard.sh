#!/bin/bash

echo Machine name : $(hostname)
echo OS : $(( lsb_release -ds || cat /etc/*release || uname -om ) 2>/dev/null | head -n1
) and kernel version is $(uname -r)
echo IP : $(hostname -I | awk '{print $1}')
echo RAM : $(grep MemFree /proc/meminfo) RAM restante sur $(grep MemTotal /proc/meminfo) RAM totale
echo disk : $(df -h | awk -v OFS='\n' '{$1=$1}1' | head -n +23 | tail -n -1) space left
echo Top 5 processes by RAM usage :
echo - $(ps -eo cmd,%mem --sort=-%mem | head -n 2 | tail -n 1 | grep -h "/")
echo - $(ps -eo cmd,%mem --sort=-%mem | head -n 3 | tail -n 1 | grep -h "/")
echo - $(ps -eo cmd,%mem --sort=-%mem | head -n 4 | tail -n 1 | grep -h "/")
echo - $(ps -eo cmd,%mem --sort=-%mem | head -n 5 | tail -n 1 | grep -h "/")
echo - $(ps -eo cmd,%mem --sort=-%mem | head -n 6 | tail -n 1 | grep -h "/")
echo Listening ports :
echo $(lsof -i -P -n | grep LISTEN | awk '{print $9}' | sed 's/*://') : $(lsof -i -P -n | grep LISTEN | awk '{print $1}' | sed 's/*://')

echo Here \'s your random cat : $(curl -s --request GET 'https://api.thecatapi.com/v1/images/search?format=json' | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*" | sort -u )

