
# [](#)TP3 : A little script

## I. Script carte d'identité

```
bash idcard.sh
Machine name : Linux-Ethan
OS : Ubuntu 20.04.3 LTS and kernel version is 5.11.0-40-generic
IP : 10.131.53.86
RAM : MemFree: 250064 kB RAM restante sur MemTotal: 8054964 kB RAM totale
disk : 292G space left
Top 5 processes by RAM usage :
- /snap/discord/130/usr/share 5.3
- /opt/brave.com/brave/brave 5.2
- /usr/bin/gnome-software --g 4.0
- /usr/bin/gnome-shell 3.2
- /opt/brave.com/brave/brave 3.2
Listening ports :
127.0.0.1:6463 : Discord
Here 's your random cat : https://cdn2.thecatapi.com/images/716.jpg

```
## II. Script youtube-dl

```
sudo bash yt.sh https://youtu.be/lIZuYic2M1A\~
Video https://youtu.be/lIZuYic2M1A~ was downloaded.
Path : /srv/yt/downloads/TEST VIDEO [360p].mp4/TEST VIDEO [360p].mp4.mp4
```

![](img/ytdl.gif)

```
cat /var/log/yt/download.log
[11/20/21 16:03:49] Video https://youtu.be/lIZuYic2M1A~ was downloaded. File path : /srv/yt/downloads/TEST VIDEO [360p].mp4/TEST VIDEO [360p].mp4.mp4
```
