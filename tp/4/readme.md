# TP4 : Une distribution orientée serveur

### Choisir et définir une ip
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:75:a8:ac brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85836sec preferred_lft 85836sec
    inet6 fe80::a00:27ff:fe75:a8ac/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:54:77:31 brd ff:ff:ff:ff:ff:ff
    inet 10.200.1.69/24 brd 10.200.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe54:7731/64 scope link
       valid_lft forever preferred_lft forever
   ```
   l'ip de l'interface host only est 10.200.1.69/24
   
   L'ip de ma machi est 10.200.1.1/24
   Ils sont donc dans le même réseaux et peuvent communiquer sans soucis.
   
   ### **Connexion SSH fonctionnelle**
   
   ![](img/ssh.gif)
**Teste de la connexion à internet**
```
[ethan@node1 ~]$ ping 1.1.1.1 -c 4
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=10.8 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=11.3 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=14.6 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=63 time=12.2 ms

--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 10.764/12.224/14.618/1.476 ms
```
**Teste de la résolution de nom**

```
[ethan@node1 ~]$ ping mangas-origines.fr -c 4
PING mangas-origines.fr (104.26.3.27) 56(84) bytes of data.
64 bytes from 104.26.3.27 (104.26.3.27): icmp_seq=1 ttl=63 time=12.1 ms
64 bytes from 104.26.3.27 (104.26.3.27): icmp_seq=2 ttl=63 time=19.5 ms
64 bytes from 104.26.3.27 (104.26.3.27): icmp_seq=3 ttl=63 time=54.2 ms
64 bytes from 104.26.3.27 (104.26.3.27): icmp_seq=4 ttl=63 time=14.6 ms

--- mangas-origines.fr ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 12.051/25.071/54.220/17.039 ms
```

### Nommer la machine

```
[ethan@node1 ~]$ cat /etc/hostname
node1.tp4.linux
[ethan@node1 ~]$ hostname
node1.tp4.linux
```

**Vérifier que NGINX est bien installé et fonctionne**

```
[ethan@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 16:10:44 CET; 1s ago
  Process: 1661 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1659 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1658 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1663 (nginx)
    Tasks: 2 (limit: 11395)
   Memory: 13.9M
   CGroup: /system.slice/nginx.service
           ├─1663 nginx: master process /usr/sbin/nginx
           └─1664 nginx: worker process

nov. 24 16:10:43 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
nov. 24 16:10:44 node1.tp4.linux nginx[1659]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nov. 24 16:10:44 node1.tp4.linux nginx[1659]: nginx: configuration file /etc/nginx/nginx.conf test is successful
nov. 24 16:10:44 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argum>
nov. 24 16:10:44 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```
**Analysez le service NGINX**
```
[ethan@node1 ~]$ ps aux | grep nginx
root        1798  0.0  0.1 121288  2188 ?        Ss   16:22   0:00 nginx: master process /usr/sbin/nginx
root        1799  0.0  0.4 151820  8052 ?        S    16:22   0:00 nginx: worker process
[ethan@node1 ~]$ sudo ss -l -p -n -u -t | grep nginx
tcp   LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1770,fd=8),("nginx",pid=1769,fd=8))
tcp   LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=1770,fd=9),("nginx",pid=1769,fd=9))

[ethan@node1 ~]$ cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user root;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;
events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
[...]
```
```
[ethan@node1 /]$ cd /usr/share/nginx/html
[ethan@node1 html]$ ls -l
total 20
-rw-r--r--. 1 root root 3332 10 juin  11:09 404.html
-rw-r--r--. 1 root root 3404 10 juin  11:09 50x.html
-rw-r--r--. 1 root root 3429 10 juin  11:09 index.old
-rw-r--r--. 1 root root  368 10 juin  11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 10 juin  11:09 poweredby.png
```
**Configuration du firewall**
```
[ethan@node1 html]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
Connection au site web

```
curl 10.200.1.69:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtPilll/DTD/xhtml11.dtd
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
/*<![CDATA[*/
body {
background-color: #fff;
color: #000;
font-size: 0.9em;
font-fapiily: sans-serif, helvettica;
margin: O;
padding: 0;
}
:link { color: #c0O;
}
```
##[](#5-modif-de-la-conf-du-serveur-web)5. Modif de la conf du serveur web
```
[ethan@node1 html]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
```
[ethan@node1 html]$ sudo ss -l -p -n -u -t | grep nginx
tcp   LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=2180,fd=9),("nginx",pid=2179,fd=9))
tcp   LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=2180,fd=10),("nginx",pid=2179,fd=10))
```
 ![](img/curl.png)
## Changer l'utilisateur du service
```
[ethan@node1 html]$ ps aux | grep nginx
root        2226  0.0  0.1 119192  2144 ?        Ss   16:43   0:00 nginx: master process /usr/sbin/nginx
web         2227  0.0  0.4 151820  8124 ?        S    16:43   0:00 nginx: worker process
```
